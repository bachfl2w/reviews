-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 17, 2018 lúc 11:28 PM
-- Phiên bản máy phục vụ: 10.1.25-MariaDB
-- Phiên bản PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `reviews`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `activations`
--

CREATE TABLE `activations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `activations`
--

INSERT INTO `activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'FVHLzNsQhZPr2g84h6F83uUsw1gHAWPN', 1, '2018-04-26 00:31:17', '2018-04-26 00:31:17', '2018-04-26 00:31:17'),
(2, 2, 'lgzCSlTQmHbb8mqU3RvJBrYXvNGLgaNc', 1, '2018-04-26 00:31:18', '2018-04-26 00:31:18', '2018-04-26 00:31:18'),
(3, 4, '02xUYYqPzhuw8EBJcp9pBE0epj1jdTB4', 1, '2018-05-09 02:34:08', '2018-05-09 02:34:08', '2018-05-09 02:34:08'),
(4, 5, 'imvmFIBk9wcmPyOlqlgRiQ1ED12vc2se', 1, '2018-05-09 03:23:27', '2018-05-09 03:23:27', '2018-05-09 03:23:27'),
(5, 6, 'At6yFRPdsa8VPZKR6GivBHyHSSKJeziy', 1, '2018-05-09 03:51:15', '2018-05-09 03:51:15', '2018-05-09 03:51:15'),
(6, 7, 'IkbZjfqt35fxu8cG92574d0thyOQwpcH', 1, '2018-05-13 16:00:23', '2018-05-13 16:00:23', '2018-05-13 16:00:23');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent`) VALUES
(1, 'Shopping', 0),
(2, 'Drinking Eating', 0),
(3, 'Travel', 0),
(4, 'Wedding', 0),
(5, 'Beauty and Health', 0),
(6, 'Entertaiment', 0),
(8, 'Mall', 1),
(9, 'Super Market', 1),
(10, 'Store', 1),
(11, 'Cafe', 2),
(12, 'Buffet', 2),
(13, 'Bar/Pub', 2),
(14, 'Beer Club', 2),
(15, 'Restaurent', 2),
(16, 'Resoft', 3),
(17, 'Hotel', 3),
(18, 'Airplane', 3),
(19, 'Tour', 3),
(20, 'Photo Wedding', 4),
(21, 'Host Wedding', 4),
(22, 'Make Up', 4),
(23, 'Uniform', 4),
(24, 'Spa Massage', 5),
(25, 'Skin Caring', 5),
(26, 'Mi pham', 5),
(27, 'Gaming Home', 6),
(28, 'Lazzer Gun', 6),
(29, 'Check in Place', 6),
(30, 'Foody', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `place_id` int(11) NOT NULL,
  `rate` double(8,1) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `place_id`, `rate`, `content`, `created_at`, `updated_at`) VALUES
(10, 4, 4, 4.0, 'Cấu hình máy oke , Khá thư giãn thoải mái ^^ 1 LIKE nhẹ', '2018-05-14 09:51:23', '2018-05-14 09:51:23'),
(11, 5, 4, 5.0, 'Nice su game cấu hình đầy đủ. Phục vụ khá nhiệu tình ha, Sẽ đến vào lần sau ^^', '2018-05-14 09:59:20', '2018-05-14 09:59:20'),
(12, 4, 4, 3.0, 'So bad. The Gaming dont have PUBG >_______<', '2018-05-14 11:35:22', '2018-05-14 11:35:22'),
(13, 5, 12, 4.0, 'nice ^^ Biển đẹp quá , sẽ đến quay lại với gia đình mềnh', '2018-05-16 08:29:47', '2018-05-16 08:29:47'),
(21, 4, 4, 5.0, 'Nice best place for gaming', '2018-05-17 06:49:14', '2018-05-17 06:49:14'),
(22, 4, 5, 3.5, 'Pizza ngon qua phuc vu nhiet tinh` nua~ cho 5 saooooooooo <3', '2018-05-17 06:50:39', '2018-05-17 06:50:39'),
(23, 4, 6, 4.0, 'Nice view dep nice music and beautyfull girl <3', '2018-05-17 06:53:07', '2018-05-17 06:53:07'),
(26, 4, 5, 3.5, 'ưeqwewqe', '2018-05-17 12:00:50', '2018-05-17 12:00:50'),
(27, 4, 5, 3.5, 'sdasdsadsadas', '2018-05-17 12:02:17', '2018-05-17 12:02:17'),
(28, 4, 5, 3.5, 'đâsdad', '2018-05-17 12:10:39', '2018-05-17 12:10:39'),
(29, 4, 5, 3.5, 'ádasdadasd', '2018-05-17 12:14:40', '2018-05-17 12:14:40'),
(30, 4, 5, 3.5, 'ádadadad', '2018-05-17 12:24:35', '2018-05-17 12:24:35'),
(31, 4, 5, 3.5, 'ádasdasdasd', '2018-05-17 12:25:20', '2018-05-17 12:25:20'),
(32, 4, 5, 3.5, 'ádasdasdasd', '2018-05-17 12:30:41', '2018-05-17 12:30:41');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `place_id` int(11) NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `images`
--

INSERT INTO `images` (`id`, `place_id`, `url`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 4, '6IaAT1HdVWrb6Ie94WV0uMxEliRELBoFkKd2AqNT.jpeg', 0, 4, '2018-05-11 07:14:59', '2018-05-11 07:14:59'),
(2, 4, 'nbVSAeN6pxHowESWk3vrjOTtZMogRHUFZatVJs8w.jpeg', 0, 4, '2018-05-11 07:14:59', '2018-05-11 07:14:59'),
(3, 4, 'McoNsD0dJK4tpmSucIT384qDYU07ZTi7JnaDj9yG.jpeg', 0, 4, '2018-05-11 07:15:00', '2018-05-11 07:15:00'),
(4, 4, 'hi4gDmazJNFZzrpWwrcPiXW9rq2PETid2bMbTvqb.jpeg', 0, 4, '2018-05-11 07:15:00', '2018-05-11 07:15:00'),
(5, 4, '6ahZpkMACWxmxYGbV1mPYfJ0fk6a6pNUFIlrwkTQ.jpeg', 0, 4, '2018-05-11 07:15:00', '2018-05-11 07:15:00'),
(6, 4, 'f3RhUE2gg1NRAUFpCsyyOtNU2czEiKSXuPoSfxxg.jpeg', 0, 4, '2018-05-11 07:15:00', '2018-05-11 07:15:00'),
(7, 5, 'bnAL6bnVWCRp1e5UTD0BKkdjnsPM6SQIWeDVVvVP.jpeg', 0, 4, '2018-05-11 07:19:41', '2018-05-11 07:19:41'),
(8, 5, 'ot7P6hOyQANmi6hN8z9cT3hhaBqq4Eb0p4uvNyf4.jpeg', 0, 4, '2018-05-11 07:19:41', '2018-05-11 07:19:41'),
(9, 5, 'ifJ9uSZfbja6scoPhioThySH1LEsYrg2ItLYpGi8.jpeg', 0, 4, '2018-05-11 07:19:41', '2018-05-11 07:19:41'),
(10, 5, '5dOAT9MnEooaAS8RHN8SC1pVTq2pgXt0jYjUgORw.jpeg', 0, 4, '2018-05-11 07:19:41', '2018-05-11 07:19:41'),
(11, 5, 'amDkHgR4Ajlu7ZreZQpAsmfLclGNpjP6EsifzfXy.jpeg', 0, 4, '2018-05-11 07:19:41', '2018-05-11 07:19:41'),
(12, 6, 'KX7i7frVGrR8k4owfmJex04ATDEardgjzcFkCcyq.jpeg', 0, 4, '2018-05-11 07:25:41', '2018-05-11 07:25:41'),
(13, 6, 'c1YsFXKe6XrdWjo7chiMK6sdPELXGWjneybKU97j.jpeg', 0, 4, '2018-05-11 07:25:41', '2018-05-11 07:25:41'),
(14, 6, 'xXRWkO86ZHEmOKRwYegyyrBRAa1k8nIihdhyvuRc.jpeg', 0, 4, '2018-05-11 07:25:41', '2018-05-11 07:25:41'),
(15, 6, 'YcjrVQLyEy7RCQxSHrzfKyeA9PsLHJY5qXZlEmBW.jpeg', 0, 4, '2018-05-11 07:25:41', '2018-05-11 07:25:41'),
(16, 6, 'AoYSAquuOX2aViAlh3DelrzFfDtFjDsNV5NlTirH.jpeg', 0, 4, '2018-05-11 07:25:42', '2018-05-11 07:25:42'),
(17, 6, '6Iag1EJfFcyKEz0CcO5waqKGUe3UMyPJ4V0Hv73T.jpeg', 0, 4, '2018-05-11 07:25:42', '2018-05-11 07:25:42'),
(18, 6, 'TPDGVxudg6tzNcu0PwGoxbCEFV4UMM0Yi1CfnQUv.jpeg', 0, 4, '2018-05-11 07:25:42', '2018-05-11 07:25:42'),
(19, 6, 'J95Aiv2R434F1QevZr0BsRlI8y7JRKeIr8zmdStP.jpeg', 0, 4, '2018-05-11 07:25:42', '2018-05-11 07:25:42'),
(20, 6, 'nNyOo6rKkyj8vNTJ5ZdUJmmIxxlUa59T8tZpnxhc.jpeg', 0, 4, '2018-05-11 07:25:42', '2018-05-11 07:25:42'),
(21, 6, 'cXeAAavROKlRzvGpwM7BLWf4gNCbEGhtBY6aznky.jpeg', 0, 4, '2018-05-11 07:25:42', '2018-05-11 07:25:42'),
(22, 6, 'FIgTpdM2WRKa6fnn67Xb6S6wIuNO991MSWKGBN8r.jpeg', 0, 4, '2018-05-11 07:25:42', '2018-05-11 07:25:42'),
(23, 7, 'rOVN7K2aYqdsx17rnTucEWIu9RXuJhJamBsJk9DU.jpeg', 0, 4, '2018-05-11 07:29:42', '2018-05-11 07:29:42'),
(24, 7, '3FkQeRdtwX64oO2NSYwPTITfuM93Qr7pEIs9tVkN.jpeg', 0, 4, '2018-05-11 07:29:42', '2018-05-11 07:29:42'),
(26, 7, 'RMffBjcvpD8IW2ifLDdvqmhFMmEYMYOLTlo6fbah.jpeg', 0, 4, '2018-05-11 07:29:42', '2018-05-11 07:29:42'),
(27, 7, 'f2oof19wVIK5AaSKr72Z8uxfTFzucxdS417RyNnU.jpeg', 0, 4, '2018-05-11 07:29:42', '2018-05-11 07:29:42'),
(28, 7, '8ZVhIzJYb23PJkMcCVtVYjMYU3DfFsUEWX1HUV4n.jpeg', 0, 4, '2018-05-11 07:29:42', '2018-05-11 07:29:42'),
(50, 12, 'rzoVlsoyv26PMWzTl1bW7Dhu580NAS4BjaL073FX.jpeg', 0, 5, '2018-05-11 08:01:45', '2018-05-11 08:01:45'),
(51, 12, 'L2PMRcdAu8IlwNVtWlirc8IFC0wKBCITho1glb5l.jpeg', 0, 5, '2018-05-11 08:01:46', '2018-05-11 08:01:46'),
(52, 12, 'HMLoMoFxB8W8K1SyArVWzCVdTOoSDXAoY76SBs1x.jpeg', 0, 5, '2018-05-11 08:01:46', '2018-05-11 08:01:46'),
(53, 12, 'jaHdt1AWLvwsngQsEycnTLmq1WzckUywOPoWYYgp.jpeg', 0, 5, '2018-05-11 08:01:46', '2018-05-11 08:01:46'),
(54, 12, 'tlPPpzgUlbO9F6f1NvpocYpKntM24fsluznmDwji.jpeg', 0, 5, '2018-05-11 08:01:46', '2018-05-11 08:01:46'),
(55, 12, 'Uu5oMlNRTOAHIqs6xiSo6iWDUU38cub4byrCdXvV.jpeg', 0, 5, '2018-05-11 08:01:46', '2018-05-11 08:01:46'),
(56, 12, 'gY4fPxTrBUpRhpzTTsMQrWlSSLAbvhZ48kusjPjg.jpeg', 0, 5, '2018-05-11 08:01:46', '2018-05-11 08:01:46');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_07_02_230147_migration_cartalyst_sentinel', 1),
(2, '2018_04_27_072757_create_users_address', 2),
(3, '2018_04_27_074025_create_users_images', 2),
(4, '2018_04_27_075355_create_users_upload_images', 2),
(5, '2018_04_27_075637_create_users_rates', 2),
(6, '2018_04_27_080000_create_users_services', 2),
(7, '2018_04_27_080510_create_users_place_services', 2),
(8, '2018_04_27_080731_create_users_categories', 2),
(9, '2018_04_27_081158_create_users_notification', 2),
(10, '2018_04_27_081805_create_table_message', 2),
(11, '2018_04_27_082051_create_table_comments', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `user_impact` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `persistences`
--

CREATE TABLE `persistences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `persistences`
--

INSERT INTO `persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
(1, 1, 'jviZZbMNmp8FUKIbrdsLDYXQNRES1TI5', '2018-04-26 01:21:27', '2018-04-26 01:21:27'),
(2, 1, 'xgU5RW8bNWO1Ki8lr91pvboHXWAEcytY', '2018-04-26 01:24:27', '2018-04-26 01:24:27'),
(3, 1, 'YO6U0ZVxN64RSzb2r7VpevzVEo5gTt11', '2018-04-26 01:25:17', '2018-04-26 01:25:17'),
(4, 1, 'Lurco5fRzw4ui5ardUWUcVOnkqZ9VTwT', '2018-04-26 01:26:14', '2018-04-26 01:26:14'),
(5, 1, 'mcKp8V9IoydmeNtTEeImsWEy81CEGqOy', '2018-04-26 01:26:24', '2018-04-26 01:26:24'),
(6, 1, 'kUhOk1eFBjM0dfKfDuUkOgvhRwjdVrFM', '2018-04-26 01:26:49', '2018-04-26 01:26:49'),
(7, 1, 'YjgqgegEaEiPIGMLHf4fpdcD8z57KeWf', '2018-04-26 01:30:55', '2018-04-26 01:30:55'),
(8, 2, 'HctHwo7jiMINlOLos4mevLHEX5rt4Rwn', '2018-04-26 01:31:32', '2018-04-26 01:31:32'),
(9, 1, 'PF7SO6Ey8XvyAebpEinJLncOJcbNfnfL', '2018-04-26 01:39:41', '2018-04-26 01:39:41'),
(10, 1, 'CRzXD0ltN8G36CXlW5GxASLbP1Dg8WBp', '2018-04-26 01:42:21', '2018-04-26 01:42:21'),
(11, 1, 'DPa0mvz17eFgNhuPgf4rEt56QT1S6NVY', '2018-04-26 01:43:49', '2018-04-26 01:43:49'),
(12, 1, 'B9t9MvLpTkvVHVU9ZPsd3zE2rqistURu', '2018-04-26 01:43:50', '2018-04-26 01:43:50'),
(13, 1, 'MrCyUsJzWdh6fDNtmyQXaKRD0f2XZY2g', '2018-04-26 01:44:20', '2018-04-26 01:44:20'),
(14, 1, 'sPobNJ2b3omUXfSHZImlFe28nhFqok6a', '2018-04-26 01:44:20', '2018-04-26 01:44:20'),
(15, 1, 'Wg05UJXUhjGYR2Y2pUBeMsTnfHGax8uR', '2018-04-26 01:45:33', '2018-04-26 01:45:33'),
(16, 1, 'XxCRP0aYJ71Rs9pMygHL7s1BNRGtdmcI', '2018-04-26 01:45:33', '2018-04-26 01:45:33'),
(17, 1, 'S9yzEl8aog2Aioxnmmf39ysCKptGb7jK', '2018-04-26 01:49:26', '2018-04-26 01:49:26'),
(18, 1, 'RQYqfsvdNOtbxvTTLjG1CVBjgw7mgBIz', '2018-04-26 01:49:26', '2018-04-26 01:49:26'),
(19, 1, 'PJEzzo2pmTBhDpoBKX5nN2pTGJw5MIbl', '2018-04-26 01:50:21', '2018-04-26 01:50:21'),
(20, 1, 'S21XSmxsrmuXfLpdabu6wWqHo1BfZTXE', '2018-04-26 01:50:21', '2018-04-26 01:50:21'),
(21, 1, 'VYXFTdYF0GmyUb4cWz9UdbOYhYlEoe6a', '2018-04-26 01:50:29', '2018-04-26 01:50:29'),
(22, 1, 'LZFLwrAAy4pZj52VYnM9VR6O7F09G4eE', '2018-04-26 01:50:29', '2018-04-26 01:50:29'),
(23, 1, '39AherAkIqkqrp7LI15YRjct55xnMwnZ', '2018-04-26 01:51:00', '2018-04-26 01:51:00'),
(24, 1, 'Go2RAsBGpmRTlWeNJE888NX7Q60e8b1i', '2018-04-26 01:51:00', '2018-04-26 01:51:00'),
(25, 2, 'UbKSSvgupqSDgEBgFyzsgI2TJhiOcAS3', '2018-04-26 01:52:08', '2018-04-26 01:52:08'),
(26, 2, 'fkX1gSr55by0WlnM7kkq6w13asqe0Zn3', '2018-04-26 01:52:08', '2018-04-26 01:52:08'),
(27, 2, 'onotvLp4X04KxLF7KRGUWsh7kp8EIFoA', '2018-04-26 01:53:02', '2018-04-26 01:53:02'),
(28, 2, 'N7448jNSkw8muVy7JdzHXPAXLul9cFs5', '2018-04-26 01:53:02', '2018-04-26 01:53:02'),
(29, 2, '0aotsVz026UKDV8zeO1nQMRp1tUcPHp0', '2018-04-26 01:53:35', '2018-04-26 01:53:35'),
(30, 2, 'XpnlKxxkgh2Y5tGmO1vAKwOsgleIcu8r', '2018-04-26 01:53:35', '2018-04-26 01:53:35'),
(31, 2, 'aDFmOlLysfyELhVQJbMTK9QYeIktPCV1', '2018-04-26 01:53:38', '2018-04-26 01:53:38'),
(32, 2, 'EjnJ6UBK06adfU3SA0usN8bGzrigPiiU', '2018-04-26 01:53:38', '2018-04-26 01:53:38'),
(33, 2, '17yaIbn95LDLrF1mwjUNXycqv8EEje51', '2018-04-26 01:53:40', '2018-04-26 01:53:40'),
(34, 2, 'mGqpW4B7qW1EOjywVen55t5UIg3IlZ8V', '2018-04-26 01:53:40', '2018-04-26 01:53:40'),
(35, 2, 'oVC5kE5NxRXnudNqzRq75KdMC2Iz1dvI', '2018-04-26 01:53:42', '2018-04-26 01:53:42'),
(36, 2, 'RQ4NgV5851xaiRUxMrUgqm7RjAFxqffH', '2018-04-26 01:53:42', '2018-04-26 01:53:42'),
(37, 2, 'Qrd1vHajauk3Ps2sHj3jgLZ4cM0iM3Dp', '2018-04-26 01:54:51', '2018-04-26 01:54:51'),
(38, 2, 'W9ONiSnApD3ukP5nk7MGdyVYRRf6XTG9', '2018-04-26 01:54:51', '2018-04-26 01:54:51'),
(39, 2, '4P8h0bog7k7jJ0jojmuNRH9jgOqp5Mjm', '2018-04-26 01:54:53', '2018-04-26 01:54:53'),
(40, 2, 'aheEZboQXq3ejHlCQyKJdNTBSdoQ2zHQ', '2018-04-26 01:54:53', '2018-04-26 01:54:53'),
(41, 2, 'MtYEaJcCSfm1YNsvtcvBtb9owgC4oDxr', '2018-04-26 01:54:55', '2018-04-26 01:54:55'),
(42, 2, '7Q2Trfe6ilo4HWhjGs6hOF1Gv6SgX0vs', '2018-04-26 01:54:55', '2018-04-26 01:54:55'),
(43, 2, 'rNugzY1QKXwaibwrrXz1RVGfA7tq9Zqw', '2018-04-26 01:54:56', '2018-04-26 01:54:56'),
(44, 2, 'XQSLIkwBx3RZO2RIf0PjA1NnSPryN12W', '2018-04-26 01:54:56', '2018-04-26 01:54:56'),
(45, 2, 'uC2TRsACwYIH62ZScX4lxqWzVVX4hHFg', '2018-04-26 01:54:58', '2018-04-26 01:54:58'),
(46, 2, 'hcHaATTVcjTyl3MFLr9T80cP4KrJNoRl', '2018-04-26 01:54:58', '2018-04-26 01:54:58'),
(47, 2, 'imPrtAQA4MmUH4zzTHd93RXm4ONZVLc4', '2018-04-26 01:54:59', '2018-04-26 01:54:59'),
(48, 2, 'Gl86hV57njkRyjUGa1hYeJE6eyrmFvNb', '2018-04-26 01:54:59', '2018-04-26 01:54:59'),
(49, 2, 'O9EhDWS9xswwXAuFQ1sEDGgkEe8Bm3m0', '2018-04-26 01:55:23', '2018-04-26 01:55:23'),
(50, 2, 'aDskFTIpzzrR3qM9Nq3zie0wgmuPiNeB', '2018-04-26 01:55:24', '2018-04-26 01:55:24'),
(51, 2, 'Xh8NgLInaLXbgUGuEZsLZVnschC4hh2x', '2018-04-26 01:55:45', '2018-04-26 01:55:45'),
(52, 2, 'oqj0Xwn4p7eblXYQRTdZwC0gJpxHm5f4', '2018-04-26 01:55:45', '2018-04-26 01:55:45'),
(53, 2, 'laSi0jaKJx2eWivgz4Q2dsPQNI6vfrDx', '2018-04-26 01:56:46', '2018-04-26 01:56:46'),
(54, 2, 'iu2lJWSVIDtc7iwOkRGITjQDETl6U3FT', '2018-04-26 01:56:46', '2018-04-26 01:56:46'),
(55, 2, 'vpFj5BMtI8DDOYGIDn8XHvtU90gbiRy2', '2018-04-26 01:58:33', '2018-04-26 01:58:33'),
(56, 2, 'VDihLTB0wcOIDUxMvHAMctRnTgWBILQ8', '2018-04-26 01:58:34', '2018-04-26 01:58:34'),
(57, 2, '7dM4xfVh6AhLhg9IPUxxSJPZaZwFuEzC', '2018-04-26 01:58:35', '2018-04-26 01:58:35'),
(58, 2, 'm2oOG0UNUZfXPqCm7Tk09Orp5ATvgF6Y', '2018-04-26 01:58:35', '2018-04-26 01:58:35'),
(59, 1, 'z7XPVdV7xtWpDqz4XPBMYolx9TxNLUjg', '2018-04-26 01:58:40', '2018-04-26 01:58:40'),
(60, 2, 'OuMQ3pixaa8wyu7cQvyccQRsZ8hCBS0I', '2018-04-26 01:58:41', '2018-04-26 01:58:41'),
(61, 2, 'yhqFWqfugfpOZUGREYCs50Gw0lQMrRj6', '2018-04-26 01:58:42', '2018-04-26 01:58:42'),
(62, 2, '40oZl3mprg9GLxlnSSnbRoK8Y3YYerAB', '2018-04-26 01:58:42', '2018-04-26 01:58:42'),
(63, 1, 'cG36Bgh5nPe9eXzAMQR47fKLk3q9G3zW', '2018-04-26 01:58:48', '2018-04-26 01:58:48'),
(64, 2, 'qik03OBtS12smpHdqZblTB0BA6CEojSz', '2018-04-26 01:58:48', '2018-04-26 01:58:48'),
(65, 2, 'NpZo4LwG7FPNMq1YilRqnz3Ahcu9oNGz', '2018-04-26 01:58:50', '2018-04-26 01:58:50'),
(66, 2, 'J4QLN0hv588WzDOh8RTA9PdwWvmBPiRk', '2018-04-26 01:58:50', '2018-04-26 01:58:50'),
(67, 2, 'AVs7InX0SIsjjWTdVEoyMCIk5Nc3gAPI', '2018-04-26 02:00:53', '2018-04-26 02:00:53'),
(68, 1, '8xwgRAGDahiPL8jFwRuK5DueXZz39woP', '2018-04-26 02:00:53', '2018-04-26 02:00:53'),
(69, 2, '4Ww6c7WN51jn0JoRMLeHHu0HXED1sQkw', '2018-04-26 02:01:24', '2018-04-26 02:01:24'),
(70, 1, 'Jmc2Sws7zMckXxB7Fe2m5y1N2SrtW4JR', '2018-04-26 02:01:24', '2018-04-26 02:01:24'),
(71, 2, 'CeuvoKMokwcOuqgEPC0Mg3dJqlyyqvFE', '2018-04-26 02:02:56', '2018-04-26 02:02:56'),
(72, 1, 'QFtzxIPkbuOpseuD1IjZmDGz4Ictwclf', '2018-04-26 02:02:56', '2018-04-26 02:02:56'),
(73, 2, '6AykmvMqnpGD3QqaIX1PnQVQEoGQMbaI', '2018-04-26 02:02:58', '2018-04-26 02:02:58'),
(74, 1, 'LpDiduFezw27YTV4DlxUEs0TA4eFbKkr', '2018-04-26 02:02:58', '2018-04-26 02:02:58'),
(75, 2, 'J0PsKCQK3WaXpm27URC5SM7hKWqMjvSW', '2018-04-26 02:03:24', '2018-04-26 02:03:24'),
(76, 1, 'IMwjnLc9ucrADaPee18t8h6kBZmDJMUl', '2018-04-26 02:03:25', '2018-04-26 02:03:25'),
(77, 1, 'XT9B1c0VAXawtBwnk6yJzCjSAAJCy2Vu', '2018-04-26 02:04:56', '2018-04-26 02:04:56'),
(78, 1, 'RdPhB9CUxBfY7A0fWXOh0rHKSbjUQdll', '2018-04-26 02:04:56', '2018-04-26 02:04:56'),
(79, 1, 'ZAtknSs1rfJpX9mtxlnFrjadWopjPZZ4', '2018-04-26 02:05:29', '2018-04-26 02:05:29'),
(80, 1, 'MPEWxVhiClWbghPsOsSVUlkDDxhtKsNa', '2018-04-26 02:05:29', '2018-04-26 02:05:29'),
(81, 1, 'LWSLwy2cr1kebb23Z66vfFU4ilSvdSTw', '2018-04-26 02:05:31', '2018-04-26 02:05:31'),
(82, 1, '8JTIvrH8m7UmCZa3ycD4SA0NywmiVL1X', '2018-04-26 02:05:31', '2018-04-26 02:05:31'),
(83, 1, 'xS51ml9YVCijHGAXBMJ2N2u9izQYhRKY', '2018-04-26 02:05:33', '2018-04-26 02:05:33'),
(84, 1, 'EmFQQ4uPWjoi9qJcVmGBKyO4EQQaHr88', '2018-04-26 02:05:33', '2018-04-26 02:05:33'),
(85, 1, 'FBAdcW7lUwdXGnBE5MNX7I5uNLn755BA', '2018-04-26 02:05:47', '2018-04-26 02:05:47'),
(86, 1, 'C3VwrhlHrFYVGXVg8oe4tzPEZtwoZSQS', '2018-04-26 02:05:47', '2018-04-26 02:05:47'),
(87, 1, 'IZGBa5FInrjHIDGG0BZcuLIWGf2wKfbx', '2018-04-26 02:08:20', '2018-04-26 02:08:20'),
(88, 1, 'qK5988Vy0q3E72TiptB4xfkSPFzv7j7E', '2018-04-26 02:10:24', '2018-04-26 02:10:24'),
(89, 1, 'BnSLoS0eLtUjDC0PICnobJY72D5Di8jm', '2018-04-26 02:10:24', '2018-04-26 02:10:24'),
(90, 2, 'Kg97WKalnZOA2bRna3gYUDov7pkJyGVE', '2018-04-26 02:10:33', '2018-04-26 02:10:33'),
(91, 2, 'fOexpLMN8IF91AiHSKWSJbAcXXFveeTt', '2018-04-26 02:10:34', '2018-04-26 02:10:34'),
(92, 1, 'cuvWYWDPYOfuui99wHApXn8RTrk4mmaN', '2018-04-26 02:12:05', '2018-04-26 02:12:05'),
(93, 1, 'QhqmAU2xUrI6VPnZ6jchLsuSULq9r7Xr', '2018-04-26 02:12:05', '2018-04-26 02:12:05'),
(94, 2, '1hiPcUT0OvkBapUCNYKWrGsh9O3GnIA8', '2018-04-26 02:12:13', '2018-04-26 02:12:13'),
(95, 2, 'VuhZryvmqZRMFjYJjqDVFGXXsq1eXtzW', '2018-04-26 02:12:13', '2018-04-26 02:12:13'),
(96, 1, 'z2Hf9bQZvfo3E4m8dcCcRK3izP4dhvOe', '2018-04-26 02:12:22', '2018-04-26 02:12:22'),
(97, 1, 'lFUljQJhmEBAmw8h45iYwZpkQECXFmV7', '2018-04-26 02:12:22', '2018-04-26 02:12:22'),
(98, 2, 'd6DISLb7uehPnk6eUu9YAiIJzJZZge7Q', '2018-04-26 02:12:30', '2018-04-26 02:12:30'),
(99, 2, 'n4F9XNJ2dehOXgdgbv88Jzh9a4YlE48U', '2018-04-26 02:12:30', '2018-04-26 02:12:30'),
(100, 1, 'lhZA0SHynvjGWpNPkKgMGOYKiBoScgJs', '2018-04-26 02:14:22', '2018-04-26 02:14:22'),
(101, 1, 'aILLYbo1UbvBd93u3DKiCFOQCvCzaN5l', '2018-04-26 02:14:22', '2018-04-26 02:14:22'),
(102, 1, '8WVSWJ2Ni8rgmQFvVjFbNKpMlbSGbSSN', '2018-04-26 02:17:55', '2018-04-26 02:17:55'),
(103, 1, '4KNDnUqlcDMhbJNUsESVpymYVl51LNfZ', '2018-04-26 02:17:55', '2018-04-26 02:17:55'),
(104, 1, '9RELfMvVbMxOE2xrTGryDNSJjsXaPZpE', '2018-04-26 02:19:27', '2018-04-26 02:19:27'),
(105, 1, 'EDRLexYYfItOrUaKQFnnBcYJ6gpzyu56', '2018-04-26 02:19:27', '2018-04-26 02:19:27'),
(106, 1, 'JKMvgR995mJJqfWIFXrrAUnRmRwljBGP', '2018-04-26 02:19:37', '2018-04-26 02:19:37'),
(107, 1, 'WIVXFEWkQPMNuo2Wi4PGTSKezhmYRaby', '2018-04-26 02:19:37', '2018-04-26 02:19:37'),
(108, 1, '6161e4J2TxdCzejQGUoK5SuLjqnxfRmQ', '2018-04-26 02:21:06', '2018-04-26 02:21:06'),
(109, 1, '9yD45xVn7hROY9iCjv0rKWeVwrV8rkbl', '2018-04-26 02:21:06', '2018-04-26 02:21:06'),
(110, 2, 'yOVWj4OLe7n33nxTOCqEKpJXT6zoltVN', '2018-04-26 02:21:14', '2018-04-26 02:21:14'),
(111, 2, 'dC0KkRHnP4hxCjMLe4Wgv7hx9oF57NzG', '2018-04-26 02:21:14', '2018-04-26 02:21:14'),
(112, 1, 'AyumPK3IAiD8VvTd8vc5RGdxslptDvhf', '2018-04-26 02:21:17', '2018-04-26 02:21:17'),
(113, 1, 'fiqG78iR0hNEkbwEfi6oe44qVV6qud1s', '2018-04-26 02:21:17', '2018-04-26 02:21:17'),
(114, 1, 'gRaTOwOupoe9KrKURBWHRIVHJCBLugs6', '2018-04-26 02:26:17', '2018-04-26 02:26:17'),
(115, 1, 'yLJM0Y2Tw6HXvu4dOBlGxzDc3AU5NJjZ', '2018-04-26 02:26:17', '2018-04-26 02:26:17'),
(116, 1, 'sRqb3jtAjyA4KNdNyV6ojsUwZke7lPCa', '2018-04-26 02:26:25', '2018-04-26 02:26:25'),
(117, 1, '6dTv9PhFm89UlMVwige7NcPEehqldLcX', '2018-04-26 02:26:25', '2018-04-26 02:26:25'),
(118, 1, 'PSgIWWHJ7Icx7hEVX4hDJvbAwjappMvo', '2018-04-26 03:26:19', '2018-04-26 03:26:19'),
(119, 1, 'nFVELZME6b9XUjKMwZpk259M5iBirdLP', '2018-04-26 03:26:19', '2018-04-26 03:26:19'),
(120, 1, 'lbwAp71KvuZUJo2ypOcxv0bSBPvJz1w3', '2018-04-26 03:26:39', '2018-04-26 03:26:39'),
(121, 1, '8G6gtBQU2TvEcO5G4dKZlKgy1Wqm8ayn', '2018-04-26 03:26:39', '2018-04-26 03:26:39'),
(122, 2, 'd8zbVXSgMSU5i9xS0PzjvkTICglSk3e5', '2018-04-26 03:26:51', '2018-04-26 03:26:51'),
(123, 2, '4NdLgUvC7yvl3jqrDNCXYFaumbqZsvQN', '2018-04-26 03:26:51', '2018-04-26 03:26:51'),
(124, 1, 'Epi1Yym9KF27JvmTOOFNpuZT7wGPVBOA', '2018-04-26 10:12:29', '2018-04-26 10:12:29'),
(125, 1, 'reaeWxAKW1ztWCnfJ1EuIxHE8gWLvgEk', '2018-04-26 10:12:29', '2018-04-26 10:12:29'),
(127, 1, 'V4ri8sqB4sxTsEybQdZS2eZ0QoQOv0i3', '2018-04-26 21:34:38', '2018-04-26 21:34:38'),
(129, 1, 'DTzUT4xkPU210QGiXFd7Zx07xhzRRmlf', '2018-04-26 21:43:11', '2018-04-26 21:43:11'),
(131, 1, 'Y0H17vdccLNga6M774uTacP89G6ekUGW', '2018-04-26 21:43:38', '2018-04-26 21:43:38'),
(133, 1, 'ZmVWRamUzvqAJd870RQzNDo1Ze6FwaFr', '2018-04-26 21:49:52', '2018-04-26 21:49:52'),
(135, 1, 'MSs4YlZQvAcHHsYfcAIvrq04Ycu61tfN', '2018-04-26 21:50:51', '2018-04-26 21:50:51'),
(137, 1, 'N4neAdheU7KKNwYNUwlHs23tnYLnXf7G', '2018-04-26 21:51:22', '2018-04-26 21:51:22'),
(139, 1, 'mtyYO0aEDvhv6lJJdKZBeSBucpsmVNME', '2018-04-26 21:54:45', '2018-04-26 21:54:45'),
(141, 1, 'krjgkPxdKJsqcsvHkhTUMKxMu8qWLy0j', '2018-04-26 21:54:57', '2018-04-26 21:54:57'),
(142, 1, 'eAIU41z5EqAQeHx4FqyAEUl3ml10ytnJ', '2018-04-28 09:32:31', '2018-04-28 09:32:31'),
(144, 1, 'r9Enq7qot9iWnBds7wtaqIaRPgnIBu1F', '2018-04-28 09:35:34', '2018-04-28 09:35:34'),
(146, 1, 'WXOErU3JgyPFn2oMuGEsZf7g2weeuccK', '2018-04-28 09:46:03', '2018-04-28 09:46:03'),
(148, 1, 'AflxJxArABNN9S9QI7rXlMkNeOCsEDBE', '2018-04-28 10:02:54', '2018-04-28 10:02:54'),
(149, 1, 'ENteTtRNxoC0jBJR30l8s3ka7QbmUzIt', '2018-04-28 10:02:55', '2018-04-28 10:02:55'),
(150, 2, 'heIP2DpEXXMwKftRkeib3jIcKNVbbgEt', '2018-05-02 23:44:09', '2018-05-02 23:44:09'),
(152, 1, '9LFqmh5C7pTLo23pHOS3mhzaru9Biq69', '2018-05-02 23:46:47', '2018-05-02 23:46:47'),
(154, 2, 'aDsMzC3GpVOOKf8yJgHnNWqeAYsyC4lc', '2018-05-02 23:47:54', '2018-05-02 23:47:54'),
(156, 1, 'GwDJ7zzNn9Jsp3aKK3Trk3I0ngd6er7X', '2018-05-02 23:51:18', '2018-05-02 23:51:18'),
(158, 2, '5w3aAVk4RFp3x7DB3YXPNHdvKV6hJf8r', '2018-05-02 23:51:38', '2018-05-02 23:51:38'),
(160, 1, 'mECJWY5uvy3MCZXdxFUXuYbB3yuxVDSW', '2018-05-02 23:51:58', '2018-05-02 23:51:58'),
(162, 2, 'vpkJIeUs5rlKDQ3XfzZbO6sN2cX9yesv', '2018-05-02 23:52:16', '2018-05-02 23:52:16'),
(164, 1, 'tFQfeFUm9Mw9oiqeoVQlZTZHuzcjjxC0', '2018-05-02 23:53:03', '2018-05-02 23:53:03'),
(166, 2, 'YvgguYzevwYAgxAqnz9kEJj5ZT41Q2M9', '2018-05-02 23:53:43', '2018-05-02 23:53:43'),
(167, 2, 'coa5TBbVz1n7uI57bH761nnWbZPJ8INy', '2018-05-02 23:53:43', '2018-05-02 23:53:43'),
(168, 1, 'rvb93qdgK1rLXRxWsVvG7z4HxHK4zOTi', '2018-05-03 00:57:49', '2018-05-03 00:57:49'),
(170, 1, 'O6HC9m3gYiCtDUtQUmTw6wQcJSQiRvuG', '2018-05-03 00:59:19', '2018-05-03 00:59:19'),
(172, 1, '7rhVFIPKtqIvE3ga4dbKLxNidPdTJJKL', '2018-05-03 00:59:33', '2018-05-03 00:59:33'),
(174, 2, 'prweFu4nLuupO8YF3EfQNaIiLv0pef4S', '2018-05-03 01:10:38', '2018-05-03 01:10:38'),
(176, 1, 'R4ClMwHp7GdzquLvjiGRK0ZE1syA3KCZ', '2018-05-03 01:10:52', '2018-05-03 01:10:52'),
(178, 1, 'oQv2U5UFZYPLdY404WSU5VnFy4y4rNpr', '2018-05-03 01:14:08', '2018-05-03 01:14:08'),
(180, 1, 'Mb79nxN2h0YXnMzNUFbYi9fRwYqbrWCR', '2018-05-03 01:24:17', '2018-05-03 01:24:17'),
(182, 1, '88A2yiWBmTs6529JDUbj4mKBc2VNNgvk', '2018-05-03 01:35:28', '2018-05-03 01:35:28'),
(184, 1, 'EakS4TeeC8bw1BMndPy0FLWxG9nGj3Fl', '2018-05-03 10:02:46', '2018-05-03 10:02:46'),
(185, 1, 'OwO5gFGJEEfbHwsjxxtcdKHIDZ1NiIej', '2018-05-03 10:02:46', '2018-05-03 10:02:46'),
(186, 1, 'jv9PTH4TFJPpA6G2Xk5NtoafZW1tu5C3', '2018-05-03 13:20:45', '2018-05-03 13:20:45'),
(188, 2, 'qfZUx1b07C2TwrsOPkFVsJiyN9YRpQQa', '2018-05-03 13:21:21', '2018-05-03 13:21:21'),
(190, 2, 'hfH0X5vC2YnKCBXVRhPYek0ldIyAz4kQ', '2018-05-03 13:24:55', '2018-05-03 13:24:55'),
(192, 1, '9VDkLe9y5p1Di7hzGAezgdOLqml085Eu', '2018-05-03 13:25:50', '2018-05-03 13:25:50'),
(193, 1, 'uKg4TKdGlgCN7SP9sIL6k7WxL86jLGhd', '2018-05-03 13:25:51', '2018-05-03 13:25:51'),
(194, 1, 'EWyzAUbDYxfVWIxbSaiu8Um25J9sfBDc', '2018-05-05 12:26:39', '2018-05-05 12:26:39'),
(195, 1, '5bs5g3bBfvSTSID6H11eNAqSs72kOH9d', '2018-05-05 12:26:40', '2018-05-05 12:26:40'),
(196, 1, 'mACrZRWaAqcOixgCWKusPPzB6ujiY69b', '2018-05-06 02:23:10', '2018-05-06 02:23:10'),
(197, 1, 'j0WpD7S0mVYgo4EE8i4RcZQZ3r01wH5d', '2018-05-06 02:23:10', '2018-05-06 02:23:10'),
(198, 1, 'ToLXD4eXO5j4iNVW6fKxY3nhXSglBzy7', '2018-05-06 07:14:32', '2018-05-06 07:14:32'),
(199, 1, 'Fiq7LQmy5bfUDhGOZkUlvdwcbfmw5RUZ', '2018-05-06 07:14:33', '2018-05-06 07:14:33'),
(200, 1, 'QSrAZySbA7HwNBACR2atdKMVje5fsYuk', '2018-05-06 11:50:46', '2018-05-06 11:50:46'),
(201, 1, 'cSnQE9KDbeFB6rQxBTI4qPLHYBVIKnEu', '2018-05-06 11:50:47', '2018-05-06 11:50:47'),
(202, 1, 'WHZqwyTjeZCAEEiXQnlvXaSpRIuDhOfE', '2018-05-07 06:31:35', '2018-05-07 06:31:35'),
(203, 1, 'ITEdevIIDbGFDq99xeFAxWdE0Nx5AsPx', '2018-05-07 06:31:35', '2018-05-07 06:31:35'),
(204, 1, 'tPywbncFwYpRyXHD84HiI4CvntaWaDM7', '2018-05-08 06:41:17', '2018-05-08 06:41:17'),
(206, 1, 'XjZazrhrMe3p5gb6wHhlwBKFdMcYID1v', '2018-05-08 06:43:10', '2018-05-08 06:43:10'),
(208, 1, 'G5kx862HaOZk4ZKwqhFwil8xMaADJ8Lp', '2018-05-08 06:46:56', '2018-05-08 06:46:56'),
(210, 2, 'yS6VujHzwicuoYHjJ3XQvmQNJSm5REfd', '2018-05-08 09:58:28', '2018-05-08 09:58:28'),
(212, 1, 'I1mum6A7nS2DrF2irK5qQxZj8rTrn62O', '2018-05-08 10:02:29', '2018-05-08 10:02:29'),
(214, 2, 'H6iFo9CP17F66kJj79Gmf8BHm6TWF86z', '2018-05-08 10:49:27', '2018-05-08 10:49:27'),
(216, 2, '6GwxIusYgPTeXubF7h79kWWhum57g9Zx', '2018-05-08 10:50:15', '2018-05-08 10:50:15'),
(218, 1, 'A7GGpsiuy83pXAvb2b2BdY6nN8eYkWB6', '2018-05-08 11:05:09', '2018-05-08 11:05:09'),
(220, 2, 'Nyeo909Zw6JZ87oVSLrHEsOwh6UTfaR9', '2018-05-08 11:05:44', '2018-05-08 11:05:44'),
(222, 2, 'ImOjiyxM5gOA3ZREQIgNqRwO6tosCltD', '2018-05-08 11:06:01', '2018-05-08 11:06:01'),
(224, 1, 'WNhvRYhC1yrB4NIx4gQH0KXwiQtQI38E', '2018-05-08 11:06:38', '2018-05-08 11:06:38'),
(226, 1, 'lHEQFU6I38gp2PRqW1XvV6Owt4PwmKtU', '2018-05-08 11:07:28', '2018-05-08 11:07:28'),
(228, 2, 'yxgrX7wAH4GYQcxJElhI0lmdElIUE89Z', '2018-05-08 11:07:38', '2018-05-08 11:07:38'),
(230, 2, 'bEOgjDW2aCtDOgugwJqxnKXv1vHJc3mM', '2018-05-08 11:08:41', '2018-05-08 11:08:41'),
(232, 2, 'ipICbtZdU8Lgi2dc6fyBXb1gBahUIAWf', '2018-05-08 11:09:14', '2018-05-08 11:09:14'),
(234, 2, 'rabZmTKSuFlFvDmytPDYRGSrdVehktce', '2018-05-08 11:09:34', '2018-05-08 11:09:34'),
(236, 2, 'Ql7ZZJtoF6yXJcKnEhY2adMylfyJXMYu', '2018-05-08 11:09:45', '2018-05-08 11:09:45'),
(238, 1, 'UNqJw37b5UQ6riO02gQ3lKN3QuF9XC93', '2018-05-08 11:23:03', '2018-05-08 11:23:03'),
(240, 1, 'nbzzN6LTnbLLilsQJBITcskgRynRQMRN', '2018-05-08 11:23:20', '2018-05-08 11:23:20'),
(242, 1, 'iSBIzu05YegMrUXvuHdiTLAZtQcGXrY8', '2018-05-08 11:24:04', '2018-05-08 11:24:04'),
(244, 1, 'MVhkbxl2woQaLOkU88bNlcxZFf5w1prK', '2018-05-08 11:24:18', '2018-05-08 11:24:18'),
(246, 1, 'knHkELk6y80A4wYKFo8ccLP6bP6hppHo', '2018-05-08 11:24:33', '2018-05-08 11:24:33'),
(248, 1, 'kQcNo8u5Ivq6W8JzsbwjoZOvc40TWcMA', '2018-05-08 11:24:59', '2018-05-08 11:24:59'),
(249, 1, 'nNvUKdA1MH33p5cPhBHGEK0TblVnSI52', '2018-05-08 11:24:59', '2018-05-08 11:24:59'),
(250, 1, 'Zx8W3ybD3vJFJQ8EnzQNwoMRVaSjhbFR', '2018-05-08 16:11:49', '2018-05-08 16:11:49'),
(252, 1, '1KBHrssNGItln6TrR5VcJS3x9YKuR3ku', '2018-05-08 16:12:26', '2018-05-08 16:12:26'),
(254, 1, 'WOlLRl5np1mnaLcWI3OdOewRFUavEzQD', '2018-05-08 16:40:10', '2018-05-08 16:40:10'),
(256, 1, '0fyfnUmuJXOvaJd7XkOpoFdKNECoD0Y9', '2018-05-08 17:20:57', '2018-05-08 17:20:57'),
(257, 1, 'JhI7FJbu35QuHpOkZxkxKlP8Zk0K2pVB', '2018-05-08 17:20:57', '2018-05-08 17:20:57'),
(258, 1, 'U7rgb83hcDsKhCYO0gvck1CW29H7fBIA', '2018-05-09 02:04:14', '2018-05-09 02:04:14'),
(260, 1, 'y19HcKKzNSYV9D93hWDze7reoTHA9ATw', '2018-05-09 02:07:07', '2018-05-09 02:07:07'),
(262, 1, 'NkvqBNGSwc8tWrmnvMae8uirT1ejZOOP', '2018-05-09 02:15:51', '2018-05-09 02:15:51'),
(264, 1, 'Bjsa2iucXdrvBFK7SxWocJhOr1obwQL3', '2018-05-09 02:16:58', '2018-05-09 02:16:58'),
(266, 1, 'Y7SrOGPVsi9W5cP8SQ4M4UYF9rOdqMRb', '2018-05-09 02:17:55', '2018-05-09 02:17:55'),
(268, 1, 'cBM74VEDNov3PoV71D4gwVAod6NWNbsg', '2018-05-09 02:20:45', '2018-05-09 02:20:45'),
(270, 1, 'L4FrlPTJjLqeCq6CgpRg26pas0Jag0y1', '2018-05-09 02:21:01', '2018-05-09 02:21:01'),
(273, 1, 'IlY18VM6XdXQ8qaPI5BJBCn6KAM2eaFK', '2018-05-09 02:35:47', '2018-05-09 02:35:47'),
(275, 1, 'NJ4vadzfWteRuxitc4bELkfugdssLvBb', '2018-05-09 02:37:02', '2018-05-09 02:37:02'),
(279, 1, '8hHHeNlNvCVkSK5XEO6nFqZ1XPvhWjdm', '2018-05-09 03:51:55', '2018-05-09 03:51:55'),
(280, 4, 'Rji2RNUEjHpEh5iSxXik9WX1fFUtkiQp', '2018-05-09 06:34:44', '2018-05-09 06:34:44'),
(281, 4, 'I2MvCuhOFZEsXJ0cOLryonQ9dja1QXm8', '2018-05-09 06:34:45', '2018-05-09 06:34:45'),
(282, 1, 'ErbeGDZBxvLrQytB2zHKd1eMhLBMKUBL', '2018-05-09 15:23:15', '2018-05-09 15:23:15'),
(284, 4, 'MEieJnv1IGv0YUB5K0cITJGd0wdWPdWp', '2018-05-09 15:23:44', '2018-05-09 15:23:44'),
(286, 4, 'q4rseykVtMna0zIrFubLlGryEZ4PbdRX', '2018-05-09 15:25:50', '2018-05-09 15:25:50'),
(288, 4, 'wjlzoNRfzj7ZIR49MAsFlfTgqlIAGvNw', '2018-05-09 16:57:46', '2018-05-09 16:57:46'),
(290, 4, 'OS4smw1YdmLwt9dwuSkO4O8jxY6SSg7V', '2018-05-09 16:58:45', '2018-05-09 16:58:45'),
(291, 4, 'iRIB29NFD4i4o4IXKbIC546ZQsgF0yGE', '2018-05-09 16:58:45', '2018-05-09 16:58:45'),
(292, 4, 'Z2z6DYjIhoZYJOvND78GjSZF9JBKYJnp', '2018-05-10 04:16:37', '2018-05-10 04:16:37'),
(294, 4, 'VtsykJM2BpNVgShlTeMvtHoqwZjqWzxC', '2018-05-10 09:51:06', '2018-05-10 09:51:06'),
(295, 4, 'gGuDZW4djmeE8I7AoJax76JV9KzE2GOO', '2018-05-10 09:51:06', '2018-05-10 09:51:06'),
(296, 1, 'TnTUB4zndJbPNrLB56Qo50sN759KH3l7', '2018-05-10 19:25:00', '2018-05-10 19:25:00'),
(298, 4, 's1kEbB9PfXlD64jcxTmwKvoAltjK58vH', '2018-05-10 20:48:44', '2018-05-10 20:48:44'),
(299, 4, '8YTR8kLYgWNA7qNWCaTvDJUIzBviKfFD', '2018-05-10 20:48:44', '2018-05-10 20:48:44'),
(300, 4, 'BHofQaGUvQObS3wVK7FiT7D47ZaUh4Ey', '2018-05-11 02:36:19', '2018-05-11 02:36:19'),
(301, 4, 'ALsSKYZYg3ke3uZTGQDwW2AXzl981NCQ', '2018-05-11 02:36:20', '2018-05-11 02:36:20'),
(302, 4, 'hYIVPLwkScKohEbEaiNrhzMPD4FANDsq', '2018-05-11 07:04:25', '2018-05-11 07:04:25'),
(304, 5, 'dqmH6T2kMzvx2JmzUwX6wh0tIhdeJMU0', '2018-05-11 07:30:14', '2018-05-11 07:30:14'),
(305, 5, 'xAG6AKGOqyMIFke4TioZKrQpza0a983c', '2018-05-11 07:30:14', '2018-05-11 07:30:14'),
(306, 4, 'gLH0dzkmks4T3cEycBnJmIFQV04ZYe2w', '2018-05-11 13:33:23', '2018-05-11 13:33:23'),
(307, 4, '64nSILTOnw4PvyoyfgJfKtXmZgE7Joqi', '2018-05-11 13:33:23', '2018-05-11 13:33:23'),
(308, 4, '5LFUxYmw3bdTKNd2leaY4fDBBG0b2K4F', '2018-05-12 02:24:28', '2018-05-12 02:24:28'),
(309, 4, 'woy6yyjpiLwQrxTzYj0wltWrWv29yEoy', '2018-05-12 02:24:28', '2018-05-12 02:24:28'),
(310, 4, '1NyhFVatH4aiph9mMPepf5iM8Q3G483w', '2018-05-12 06:44:13', '2018-05-12 06:44:13'),
(311, 4, 'ppOE6HXMocse5My5p9lJfxEjC94v0177', '2018-05-12 06:44:13', '2018-05-12 06:44:13'),
(312, 4, 'tInv0zE81OmX2l47D7K9cxqAK4LcF3GC', '2018-05-12 14:40:10', '2018-05-12 14:40:10'),
(313, 4, 'tFlCI42RaYO3Hi14lyG9XHiRNmoXH47n', '2018-05-12 14:40:10', '2018-05-12 14:40:10'),
(314, 4, '8WBMPaRmcieuapcnKEECgXDQMbUjXJ9j', '2018-05-12 16:26:37', '2018-05-12 16:26:37'),
(315, 4, 'orR9NYHqewQhWZLgFycnGZDr9GN8aLq6', '2018-05-12 16:26:37', '2018-05-12 16:26:37'),
(316, 4, 'G29jGODRuBVfIBObLXIdzJONEDzwXruu', '2018-05-13 15:52:01', '2018-05-13 15:52:01'),
(319, 4, 'wFqKkYzeVxZn1ZGVSvHHUWlV7U5HBtmQ', '2018-05-13 16:16:00', '2018-05-13 16:16:00'),
(321, 4, 'b2uEt7qeaYByVMmfttaA6Mo7RFcn8Mvy', '2018-05-13 16:56:19', '2018-05-13 16:56:19'),
(323, 4, 'nQyjA0xAbtjSbaURJkdeFm2MTqxuYOIq', '2018-05-13 18:19:28', '2018-05-13 18:19:28'),
(324, 4, 'ZxvOVnYP6EfWkWzwfjbHLuCHe1bgQAwc', '2018-05-13 18:19:28', '2018-05-13 18:19:28'),
(325, 4, 'Ao9pFO3vOYFnuxuheKOtDtj04ABxJ2o4', '2018-05-14 06:52:19', '2018-05-14 06:52:19'),
(327, 4, 'u5QDcWl8RgdYBfQhyLhZK0D0hlrQpxMy', '2018-05-14 07:22:22', '2018-05-14 07:22:22'),
(329, 5, 'TIkky9LlqoebF40uOFZoE4xsGE7iPzpA', '2018-05-14 09:58:25', '2018-05-14 09:58:25'),
(331, 4, 'Y9ouF0Nc3ZF4qoqnPCgCza0YO5r2KLn2', '2018-05-14 11:34:37', '2018-05-14 11:34:37'),
(332, 4, '2ERznJZel4IP3iZacKESCTOwfE1y8tlo', '2018-05-14 11:34:37', '2018-05-14 11:34:37'),
(333, 1, 'uEudfLVQZARNrEKGyvSOITzZclEs1Mdy', '2018-05-15 05:26:58', '2018-05-15 05:26:58'),
(334, 1, '9P5V3hEMyFTsX4w8pV0t4G3gd3b3KSHt', '2018-05-15 05:26:58', '2018-05-15 05:26:58'),
(335, 5, 'fnAHebe3zCs3e7FkHAIdF060WLBPcTKx', '2018-05-16 08:29:19', '2018-05-16 08:29:19'),
(337, 4, 'V9h3cjgev8BjSjNSWsda1JozBYZlXKjz', '2018-05-16 10:24:48', '2018-05-16 10:24:48'),
(338, 4, '5rfyUYHrSr4uq9s69HleQnUcyIooEyBJ', '2018-05-16 10:24:49', '2018-05-16 10:24:49'),
(339, 4, '9UzsC8ETYRxJB5NhHdVy2jU86gO0TPtE', '2018-05-17 03:26:11', '2018-05-17 03:26:11'),
(341, 4, '0lwHmMIjOzZ7g8w70qZxkrFqFyYgFMAd', '2018-05-17 06:35:45', '2018-05-17 06:35:45'),
(343, 4, 'qsu2n6UV90fVE0oTHWkYBwJKnR6OEnDC', '2018-05-17 06:51:52', '2018-05-17 06:51:52'),
(344, 4, 'ISkqbr0qspxJrmCDfvTvDKWVe91FEK1A', '2018-05-17 06:51:53', '2018-05-17 06:51:53'),
(345, 4, 'vmM4SKmsi9eZ0zyjrskIQ5SJReL3Jn8U', '2018-05-17 23:01:07', '2018-05-17 23:01:07'),
(346, 4, 'd15Ay7htuXRvrr4YgecilZGvxsg3wHvx', '2018-05-17 23:01:07', '2018-05-17 23:01:07');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `places`
--

CREATE TABLE `places` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` double(8,2) NOT NULL,
  `lng` double(8,2) NOT NULL,
  `author_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `district` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `open` time NOT NULL,
  `close` time NOT NULL,
  `count_views` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `places`
--

INSERT INTO `places` (`id`, `name`, `lat`, `lng`, `author_id`, `status`, `category_id`, `city`, `district`, `phone`, `website`, `description`, `open`, `close`, `count_views`, `created_at`, `updated_at`) VALUES
(4, 'Gaming Home', 21.03, 105.78, 4, 0, 27, 'Hà Nội', 'Quận Cầu Giấy', '0977137018', 'gaming.com', 'A LAN Gaming Center is a business where one can use a computer connected over a LAN to other computers, primarily for the purpose of playing multiplayer computer games.', '01:00:00', '24:00:00', 89, '2018-05-11 07:14:59', '2018-05-17 23:17:04'),
(5, 'Pizza Ngon Nhat He Mat Troi', 20.99, 105.86, 4, 0, 30, 'Hà Nội', 'Quận Hoàng Mai', '977137018', 'pizzza.com', 'Pizza is a traditional Italian dish consisting of a yeasted flatbread typically topped with tomato sauce and cheese and baked in an oven. It can also be topped with additional vegetables, meats, and condiments, and can be made without cheese.', '01:00:00', '02:00:00', 52, '2018-05-11 07:19:41', '2018-05-17 12:32:13'),
(6, 'Crush Cafe', 20.98, 105.79, 4, 0, 11, 'Hà Nội', 'Quận Hà Đông', '977137018', 'crush_cafe.com', 'Crush Everybody , u can find your true love here good luck to all. -20% for couple', '08:00:00', '19:00:00', 149, '2018-05-11 07:25:41', '2018-05-17 11:18:41'),
(7, 'Beer Viking', 21.00, 105.79, 4, 0, 14, 'Hà Nội', 'Quận Thanh Xuân', '0977137018', 'beerviking.com', 'Viking Beer is an Icelandic brand, brewed by Vifilfell hf. The brewery is located in Akureyri, Iceland, a small town just south of the Arctic Circle. The brand name is derived from the Vikings – they were seafarers and warriors who inhabited most of Northern Europe within the period of 800 to 1200 A.D.', '10:00:00', '20:00:00', 1, '2018-05-11 07:29:42', '2018-05-16 08:14:49'),
(12, 'Nha Trang Resoft', 20.44, 106.59, 5, 0, 16, 'Khánh Hòa', 'Thành phố Nha Trang', '977137018', 'nhatrangbeach.com', 'Tran Phu Beach is easy to walk to from almost anywhere in Nha Trang City, making it the most popular beach in Nha Trang. It hosts the iconic Tran Phu Street, which is a beautiful seaside promenade with plenty of luxury and mid-range beachfront resorts, hostels, souvenir shops, museums, and seafood restaurants.', '08:00:00', '16:00:00', 9, '2018-05-11 08:01:45', '2018-05-16 08:48:54');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `place_services`
--

CREATE TABLE `place_services` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_id` int(11) NOT NULL,
  `place_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `place_services`
--

INSERT INTO `place_services` (`id`, `service_id`, `place_id`) VALUES
(5, 1, 4),
(6, 3, 4),
(7, 4, 4),
(8, 5, 4),
(9, 1, 5),
(10, 1, 6),
(11, 5, 6),
(12, 7, 6),
(13, 1, 7),
(14, 4, 7),
(15, 6, 7),
(16, 7, 7),
(25, 1, 12),
(26, 3, 12),
(27, 4, 12),
(28, 6, 12);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `rates`
--

CREATE TABLE `rates` (
  `id` int(10) UNSIGNED NOT NULL,
  `place_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `star` double(8,1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `rates`
--

INSERT INTO `rates` (`id`, `place_id`, `user_id`, `star`, `created_at`, `updated_at`) VALUES
(7, 4, 5, 5.0, '2018-05-14 09:59:20', '2018-05-14 09:59:20'),
(8, 12, 5, 4.0, '2018-05-16 08:29:47', '2018-05-16 08:29:47'),
(9, 4, 4, 5.0, '2018-05-15 17:00:00', '2018-05-17 06:49:14'),
(10, 5, 4, 3.5, '2018-05-17 06:50:39', '2018-05-17 11:54:52');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `reminders`
--

CREATE TABLE `reminders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', NULL, '2018-04-26 00:31:17', '2018-04-26 00:31:17'),
(2, 'collab', 'Collab', NULL, '2018-04-26 00:31:17', '2018-04-26 00:31:17'),
(3, 'member', 'Member', NULL, '2018-04-26 00:31:17', '2018-04-26 00:31:17');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role_users`
--

CREATE TABLE `role_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `role_users`
--

INSERT INTO `role_users` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-05-07 07:03:36', '2018-05-07 07:03:36'),
(2, 2, '2018-05-07 06:47:41', '2018-05-07 06:47:41'),
(4, 3, '2018-05-09 02:34:08', '2018-05-09 02:34:08'),
(5, 3, '2018-05-09 03:23:27', '2018-05-09 03:23:27'),
(6, 3, '2018-05-09 03:51:15', '2018-05-09 03:51:15'),
(7, 3, '2018-05-13 16:00:23', '2018-05-13 16:00:23');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `services`
--

INSERT INTO `services` (`id`, `name`) VALUES
(1, 'Wifi'),
(2, 'Online Reservation'),
(3, 'Events'),
(4, 'Host'),
(5, 'Gaming House'),
(6, 'Event'),
(7, 'Park Center');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `throttle`
--

CREATE TABLE `throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `type`, `ip`, `created_at`, `updated_at`) VALUES
(395, NULL, 'global', NULL, '2018-04-26 11:28:43', '2018-04-26 11:28:43'),
(396, NULL, 'ip', '127.0.0.1', '2018-04-26 11:28:44', '2018-04-26 11:28:44'),
(397, NULL, 'global', NULL, '2018-04-26 11:28:45', '2018-04-26 11:28:45'),
(398, NULL, 'ip', '127.0.0.1', '2018-04-26 11:28:45', '2018-04-26 11:28:45'),
(399, NULL, 'global', NULL, '2018-04-26 11:28:46', '2018-04-26 11:28:46'),
(400, NULL, 'ip', '127.0.0.1', '2018-04-26 11:28:46', '2018-04-26 11:28:46'),
(401, NULL, 'global', NULL, '2018-04-26 11:28:47', '2018-04-26 11:28:47'),
(402, NULL, 'ip', '127.0.0.1', '2018-04-26 11:28:47', '2018-04-26 11:28:47'),
(403, NULL, 'global', NULL, '2018-04-26 11:28:48', '2018-04-26 11:28:48'),
(404, NULL, 'ip', '127.0.0.1', '2018-04-26 11:28:48', '2018-04-26 11:28:48'),
(405, NULL, 'global', NULL, '2018-04-26 11:28:49', '2018-04-26 11:28:49'),
(406, NULL, 'ip', '127.0.0.1', '2018-04-26 11:28:49', '2018-04-26 11:28:49'),
(407, NULL, 'global', NULL, '2018-04-26 11:28:50', '2018-04-26 11:28:50'),
(408, NULL, 'ip', '127.0.0.1', '2018-04-26 11:28:50', '2018-04-26 11:28:50'),
(409, NULL, 'global', NULL, '2018-04-26 11:28:51', '2018-04-26 11:28:51'),
(410, NULL, 'ip', '127.0.0.1', '2018-04-26 11:28:51', '2018-04-26 11:28:51'),
(411, NULL, 'global', NULL, '2018-04-26 11:28:52', '2018-04-26 11:28:52'),
(412, NULL, 'ip', '127.0.0.1', '2018-04-26 11:28:52', '2018-04-26 11:28:52'),
(413, NULL, 'global', NULL, '2018-04-26 11:28:53', '2018-04-26 11:28:53'),
(414, NULL, 'ip', '127.0.0.1', '2018-04-26 11:28:53', '2018-04-26 11:28:53'),
(415, NULL, 'global', NULL, '2018-04-26 11:35:48', '2018-04-26 11:35:48'),
(416, NULL, 'ip', '127.0.0.1', '2018-04-26 11:35:48', '2018-04-26 11:35:48'),
(417, NULL, 'global', NULL, '2018-04-26 11:41:31', '2018-04-26 11:41:31'),
(418, NULL, 'ip', '127.0.0.1', '2018-04-26 11:41:31', '2018-04-26 11:41:31'),
(419, NULL, 'global', NULL, '2018-04-26 11:43:15', '2018-04-26 11:43:15'),
(420, NULL, 'ip', '127.0.0.1', '2018-04-26 11:43:15', '2018-04-26 11:43:15'),
(421, NULL, 'global', NULL, '2018-04-26 11:59:48', '2018-04-26 11:59:48'),
(422, NULL, 'ip', '127.0.0.1', '2018-04-26 11:59:48', '2018-04-26 11:59:48'),
(423, NULL, 'global', NULL, '2018-04-26 12:18:39', '2018-04-26 12:18:39'),
(424, NULL, 'ip', '127.0.0.1', '2018-04-26 12:18:39', '2018-04-26 12:18:39'),
(425, NULL, 'global', NULL, '2018-04-26 21:49:51', '2018-04-26 21:49:51'),
(426, NULL, 'ip', '127.0.0.1', '2018-04-26 21:49:51', '2018-04-26 21:49:51'),
(427, NULL, 'global', NULL, '2018-04-26 21:50:09', '2018-04-26 21:50:09'),
(428, NULL, 'ip', '127.0.0.1', '2018-04-26 21:50:09', '2018-04-26 21:50:09'),
(429, NULL, 'global', NULL, '2018-04-26 21:50:14', '2018-04-26 21:50:14'),
(430, NULL, 'ip', '127.0.0.1', '2018-04-26 21:50:14', '2018-04-26 21:50:14'),
(431, NULL, 'global', NULL, '2018-04-26 21:50:19', '2018-04-26 21:50:19'),
(432, NULL, 'ip', '127.0.0.1', '2018-04-26 21:50:19', '2018-04-26 21:50:19'),
(433, NULL, 'global', NULL, '2018-04-26 21:50:25', '2018-04-26 21:50:25'),
(434, NULL, 'ip', '127.0.0.1', '2018-04-26 21:50:25', '2018-04-26 21:50:25'),
(435, NULL, 'global', NULL, '2018-04-26 21:50:30', '2018-04-26 21:50:30'),
(436, NULL, 'ip', '127.0.0.1', '2018-04-26 21:50:30', '2018-04-26 21:50:30'),
(437, NULL, 'global', NULL, '2018-04-26 21:50:36', '2018-04-26 21:50:36'),
(438, NULL, 'ip', '127.0.0.1', '2018-04-26 21:50:36', '2018-04-26 21:50:36'),
(439, NULL, 'global', NULL, '2018-04-26 21:52:12', '2018-04-26 21:52:12'),
(440, NULL, 'ip', '127.0.0.1', '2018-04-26 21:52:12', '2018-04-26 21:52:12'),
(441, NULL, 'global', NULL, '2018-04-26 21:54:37', '2018-04-26 21:54:37'),
(442, NULL, 'ip', '127.0.0.1', '2018-04-26 21:54:37', '2018-04-26 21:54:37'),
(443, 1, 'user', NULL, '2018-04-26 21:54:37', '2018-04-26 21:54:37'),
(444, NULL, 'global', NULL, '2018-04-26 21:54:41', '2018-04-26 21:54:41'),
(445, NULL, 'ip', '127.0.0.1', '2018-04-26 21:54:41', '2018-04-26 21:54:41'),
(446, 1, 'user', NULL, '2018-04-26 21:54:42', '2018-04-26 21:54:42'),
(447, NULL, 'global', NULL, '2018-04-26 21:55:23', '2018-04-26 21:55:23'),
(448, NULL, 'ip', '127.0.0.1', '2018-04-26 21:55:23', '2018-04-26 21:55:23'),
(449, NULL, 'global', NULL, '2018-04-26 21:55:28', '2018-04-26 21:55:28'),
(450, NULL, 'ip', '127.0.0.1', '2018-04-26 21:55:28', '2018-04-26 21:55:28'),
(451, NULL, 'global', NULL, '2018-05-02 23:43:56', '2018-05-02 23:43:56'),
(452, NULL, 'ip', '127.0.0.1', '2018-05-02 23:43:56', '2018-05-02 23:43:56'),
(453, NULL, 'global', NULL, '2018-05-08 10:32:52', '2018-05-08 10:32:52'),
(454, NULL, 'ip', '127.0.0.1', '2018-05-08 10:32:53', '2018-05-08 10:32:53'),
(455, NULL, 'global', NULL, '2018-05-08 10:32:54', '2018-05-08 10:32:54'),
(456, NULL, 'ip', '127.0.0.1', '2018-05-08 10:32:54', '2018-05-08 10:32:54'),
(457, NULL, 'global', NULL, '2018-05-08 10:32:55', '2018-05-08 10:32:55'),
(458, NULL, 'ip', '127.0.0.1', '2018-05-08 10:32:55', '2018-05-08 10:32:55'),
(459, NULL, 'global', NULL, '2018-05-08 10:33:08', '2018-05-08 10:33:08'),
(460, NULL, 'ip', '127.0.0.1', '2018-05-08 10:33:08', '2018-05-08 10:33:08'),
(461, NULL, 'global', NULL, '2018-05-08 10:33:09', '2018-05-08 10:33:09'),
(462, NULL, 'ip', '127.0.0.1', '2018-05-08 10:33:09', '2018-05-08 10:33:09'),
(463, NULL, 'global', NULL, '2018-05-08 10:33:09', '2018-05-08 10:33:09'),
(464, NULL, 'ip', '127.0.0.1', '2018-05-08 10:33:09', '2018-05-08 10:33:09'),
(465, NULL, 'global', NULL, '2018-05-08 10:49:12', '2018-05-08 10:49:12'),
(466, NULL, 'ip', '127.0.0.1', '2018-05-08 10:49:12', '2018-05-08 10:49:12'),
(467, 2, 'user', NULL, '2018-05-08 10:49:12', '2018-05-08 10:49:12'),
(468, NULL, 'global', NULL, '2018-05-08 10:49:18', '2018-05-08 10:49:18'),
(469, NULL, 'ip', '127.0.0.1', '2018-05-08 10:49:18', '2018-05-08 10:49:18'),
(470, 2, 'user', NULL, '2018-05-08 10:49:19', '2018-05-08 10:49:19'),
(471, NULL, 'global', NULL, '2018-05-08 10:50:34', '2018-05-08 10:50:34'),
(472, NULL, 'ip', '127.0.0.1', '2018-05-08 10:50:34', '2018-05-08 10:50:34'),
(473, 1, 'user', NULL, '2018-05-08 10:50:34', '2018-05-08 10:50:34'),
(474, NULL, 'global', NULL, '2018-05-08 10:50:36', '2018-05-08 10:50:36'),
(475, NULL, 'ip', '127.0.0.1', '2018-05-08 10:50:36', '2018-05-08 10:50:36'),
(476, NULL, 'global', NULL, '2018-05-08 10:50:36', '2018-05-08 10:50:36'),
(477, 1, 'user', NULL, '2018-05-08 10:50:36', '2018-05-08 10:50:36'),
(478, NULL, 'ip', '127.0.0.1', '2018-05-08 10:50:36', '2018-05-08 10:50:36'),
(479, 1, 'user', NULL, '2018-05-08 10:50:36', '2018-05-08 10:50:36'),
(480, NULL, 'global', NULL, '2018-05-08 10:50:36', '2018-05-08 10:50:36'),
(481, NULL, 'ip', '127.0.0.1', '2018-05-08 10:50:37', '2018-05-08 10:50:37'),
(482, 1, 'user', NULL, '2018-05-08 10:50:37', '2018-05-08 10:50:37'),
(483, NULL, 'global', NULL, '2018-05-08 11:07:22', '2018-05-08 11:07:22'),
(484, NULL, 'ip', '127.0.0.1', '2018-05-08 11:07:22', '2018-05-08 11:07:22'),
(485, 1, 'user', NULL, '2018-05-08 11:07:22', '2018-05-08 11:07:22'),
(486, NULL, 'global', NULL, '2018-05-08 11:07:49', '2018-05-08 11:07:49'),
(487, NULL, 'ip', '127.0.0.1', '2018-05-08 11:07:49', '2018-05-08 11:07:49'),
(488, 1, 'user', NULL, '2018-05-08 11:07:49', '2018-05-08 11:07:49'),
(489, NULL, 'global', NULL, '2018-05-08 11:08:54', '2018-05-08 11:08:54'),
(490, NULL, 'ip', '127.0.0.1', '2018-05-08 11:08:54', '2018-05-08 11:08:54'),
(491, 1, 'user', NULL, '2018-05-08 11:08:54', '2018-05-08 11:08:54'),
(492, NULL, 'global', NULL, '2018-05-08 11:08:56', '2018-05-08 11:08:56'),
(493, NULL, 'ip', '127.0.0.1', '2018-05-08 11:08:56', '2018-05-08 11:08:56'),
(494, 1, 'user', NULL, '2018-05-08 11:08:56', '2018-05-08 11:08:56'),
(495, NULL, 'global', NULL, '2018-05-08 11:09:25', '2018-05-08 11:09:25'),
(496, NULL, 'ip', '127.0.0.1', '2018-05-08 11:09:25', '2018-05-08 11:09:25'),
(497, 2, 'user', NULL, '2018-05-08 11:09:25', '2018-05-08 11:09:25'),
(498, NULL, 'global', NULL, '2018-05-08 11:10:03', '2018-05-08 11:10:03'),
(499, NULL, 'ip', '127.0.0.1', '2018-05-08 11:10:03', '2018-05-08 11:10:03'),
(500, 1, 'user', NULL, '2018-05-08 11:10:03', '2018-05-08 11:10:03'),
(501, NULL, 'global', NULL, '2018-05-08 16:12:21', '2018-05-08 16:12:21'),
(502, NULL, 'ip', '127.0.0.1', '2018-05-08 16:12:21', '2018-05-08 16:12:21'),
(503, 1, 'user', NULL, '2018-05-08 16:12:21', '2018-05-08 16:12:21'),
(504, NULL, 'global', NULL, '2018-05-08 16:12:38', '2018-05-08 16:12:38'),
(505, NULL, 'ip', '127.0.0.1', '2018-05-08 16:12:38', '2018-05-08 16:12:38'),
(506, NULL, 'global', NULL, '2018-05-08 16:12:46', '2018-05-08 16:12:46'),
(507, NULL, 'ip', '127.0.0.1', '2018-05-08 16:12:46', '2018-05-08 16:12:46'),
(508, 2, 'user', NULL, '2018-05-08 16:12:46', '2018-05-08 16:12:46'),
(509, NULL, 'global', NULL, '2018-05-08 16:12:47', '2018-05-08 16:12:47'),
(510, NULL, 'ip', '127.0.0.1', '2018-05-08 16:12:47', '2018-05-08 16:12:47'),
(511, 2, 'user', NULL, '2018-05-08 16:12:47', '2018-05-08 16:12:47'),
(512, NULL, 'global', NULL, '2018-05-09 02:09:19', '2018-05-09 02:09:19'),
(513, NULL, 'ip', '127.0.0.1', '2018-05-09 02:09:19', '2018-05-09 02:09:19'),
(514, 1, 'user', NULL, '2018-05-09 02:09:19', '2018-05-09 02:09:19'),
(515, NULL, 'global', NULL, '2018-05-09 02:09:40', '2018-05-09 02:09:40'),
(516, NULL, 'ip', '127.0.0.1', '2018-05-09 02:09:40', '2018-05-09 02:09:40'),
(517, 1, 'user', NULL, '2018-05-09 02:09:40', '2018-05-09 02:09:40'),
(518, NULL, 'global', NULL, '2018-05-15 15:22:36', '2018-05-15 15:22:36'),
(519, NULL, 'ip', '127.0.0.1', '2018-05-15 15:22:37', '2018-05-15 15:22:37'),
(520, 1, 'user', NULL, '2018-05-15 15:22:37', '2018-05-15 15:22:37'),
(521, NULL, 'global', NULL, '2018-05-15 15:22:43', '2018-05-15 15:22:43'),
(522, NULL, 'ip', '127.0.0.1', '2018-05-15 15:22:43', '2018-05-15 15:22:43'),
(523, 1, 'user', NULL, '2018-05-15 15:22:43', '2018-05-15 15:22:43'),
(524, NULL, 'global', NULL, '2018-05-15 15:22:47', '2018-05-15 15:22:47'),
(525, NULL, 'ip', '127.0.0.1', '2018-05-15 15:22:47', '2018-05-15 15:22:47'),
(526, 1, 'user', NULL, '2018-05-15 15:22:47', '2018-05-15 15:22:47');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `avatar`, `permissions`, `last_login`, `first_name`, `last_name`, `created_at`, `updated_at`) VALUES
(1, 'vietnga1910@gmail.com', '$2y$10$D5to04eEfSpvDiuwyX0xPeVgot0BC8E/bg09B8LzWC5h32MdQQXfm', NULL, NULL, '2018-05-15 05:26:58', 'Viet', 'ThuNgaa', '2018-04-26 00:31:17', '2018-05-15 05:26:58'),
(2, 'minhthu@gmail.com', '$2y$10$Ab8EmMHnI5jcgsgROfOIvO8Zytpy4TTZ2uT82v3dUxTkZ36hrWseW', NULL, NULL, '2018-05-08 11:09:45', 'Minh', 'Thư', '2018-04-26 00:31:18', '2018-05-08 11:09:45'),
(4, 'yanbi@gmail.com', '$2y$10$zG0U.9cjHcgfkNaWt4kfv.chMWXaZEk2/aSUaAQ/MnRgAOxrGCQkO', NULL, NULL, '2018-05-17 23:01:07', 'Yanbi', 'YC', '2018-05-09 02:34:08', '2018-05-17 23:01:07'),
(5, 'tham@gmail.com', '$2y$10$aHNT9uKcUjnl59qhHIxj5OyhnYHO.rWhPSQ09exny1z5c.6N9GRTu', NULL, NULL, '2018-05-16 08:29:19', 'Dinh', 'Tham', '2018-05-09 03:23:27', '2018-05-16 08:29:19'),
(6, 'duongtung@gmail.com', '$2y$10$oxWMKFOX9EKbX5QRxfDyWuzm1EFATin6bSyiCnM3o0wL0hIIlrnTq', NULL, NULL, '2018-05-09 03:51:15', 'Duong', 'YC', '2018-05-09 03:51:15', '2018-05-09 03:51:15');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `activations`
--
ALTER TABLE `activations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `persistences`
--
ALTER TABLE `persistences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persistences_code_unique` (`code`);

--
-- Chỉ mục cho bảng `places`
--
ALTER TABLE `places`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `place_services`
--
ALTER TABLE `place_services`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `rates`
--
ALTER TABLE `rates`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `reminders`
--
ALTER TABLE `reminders`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Chỉ mục cho bảng `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- Chỉ mục cho bảng `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `throttle_user_id_index` (`user_id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `activations`
--
ALTER TABLE `activations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT cho bảng `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT cho bảng `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT cho bảng `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT cho bảng `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `persistences`
--
ALTER TABLE `persistences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=347;
--
-- AUTO_INCREMENT cho bảng `places`
--
ALTER TABLE `places`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT cho bảng `place_services`
--
ALTER TABLE `place_services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT cho bảng `rates`
--
ALTER TABLE `rates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT cho bảng `reminders`
--
ALTER TABLE `reminders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT cho bảng `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT cho bảng `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=527;
--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
